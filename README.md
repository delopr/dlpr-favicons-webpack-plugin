Favicons Webpack Plugin ver. DLPR
========================================

Plugin for Webpack, that is extended fork of https://github.com/jantimon/favicons-webpack-plugin. It handles new options that are available in origin project: https://github.com/evilebottnawi/favicons#usage

# License

This project is licensed under [MIT].